package calculator.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import calculator.controller.FiguraController;
import calculator.modelo.Circulo;
import calculator.modelo.Cuadrado;
import calculator.modelo.Figura;
import calculator.modelo.Rectangulo;
import calculator.modelo.Triangulo;
import calculator.modelo.TrianguloRectangulo;
import calculator.modelo.exception.FiguraException;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.Color;

public class FiguraView {

    private JFrame frame;
    private JTextField textValor;
    private JTable tablFiguras;

    private List<Figura> figuras;
    private String arrayfiguras[][];
    private Figura figuraAmodificarEliminar;
    private JTextField textNombre;
    private JTextField textBase;
    private JTextField textField_2;
    private JTextField textAltura;
    private JTextField textField_4;
    private JTextField textField_5;
    private JLabel lblSuperficieMaxima;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    FiguraView window = new FiguraView();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public FiguraView() {
        initialize();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(10, 10, 455, 638);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblNewLabel = new JLabel("Figuras geom\u00E9tricas");
        lblNewLabel.setFont(new Font("Segoe UI", Font.BOLD, 24));
        lblNewLabel.setBounds(10, 25, 262, 29);
        frame.getContentPane().add(lblNewLabel);

        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, "Una variable", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panel.setBounds(10, 124, 133, 140);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        textValor = new JTextField();
        textValor.setBounds(10, 41, 112, 20);
        panel.add(textValor);
        textValor.setFont(new Font("Tahoma", Font.BOLD, 16));
        textValor.setColumns(10);

        JButton btnCrearCuadrado = new JButton("Cuadrado");
        btnCrearCuadrado.setBounds(10, 72, 112, 26);
        panel.add(btnCrearCuadrado);
        btnCrearCuadrado.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Cuadrado cua = new Cuadrado(textNombre.getText(), Float.parseFloat(textValor.getText()));
                try {
                    figuraController.addHandler(cua);
                    figuras.add(cua);

                    llenarGrilla(figuras);
                    limpiarCampos();
                } catch (FiguraException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage());
                    e1.printStackTrace();
                }

            }
        });
        btnCrearCuadrado.setFont(new Font("Segoe UI", Font.PLAIN, 12));

        JLabel lblValor = new JLabel("Largo");
        lblValor.setBounds(10, 22, 99, 20);
        panel.add(lblValor);
        lblValor.setFont(new Font("Segoe UI", Font.PLAIN, 12));

        JButton btnCrearCirculo = new JButton("Circulo");
        btnCrearCirculo.setBounds(10, 104, 112, 25);
        panel.add(btnCrearCirculo);
        btnCrearCirculo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Circulo cir = new Circulo(textNombre.getText(), Float.parseFloat(textValor.getText()));
                try {
                    figuraController.addHandler(cir);
                    figuras.add(cir);
                } catch (FiguraException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage());
                    e1.printStackTrace();
                }

                llenarGrilla(figuras);
                limpiarCampos();
            }
        });
        btnCrearCirculo.setFont(new Font("Segoe UI", Font.PLAIN, 12));

        JButton btnModificar = new JButton("Modificar");
        btnModificar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (figuraAmodificarEliminar instanceof Cuadrado) {
                    Cuadrado cuad = (Cuadrado) figuraAmodificarEliminar;
                    cuad.setNombre(textNombre.getText());
                    cuad.setLado(Float.parseFloat(textValor.getText()));
                } else if (figuraAmodificarEliminar instanceof Circulo) {
                    Circulo cir = (Circulo) figuraAmodificarEliminar;
                    cir.setNombre(textNombre.getText());
                    cir.setRadio(Float.parseFloat(textValor.getText()));
                } else if (figuraAmodificarEliminar instanceof Rectangulo) {
                    Rectangulo rec = (Rectangulo) figuraAmodificarEliminar;
                    rec.setBase(Float.parseFloat(textBase.getText()));
                    rec.setAltura(Float.parseFloat(textAltura.getText()));
                }

                llenarGrilla(figuras);
                figuraAmodificarEliminar = null;
                limpiarCampos();
            }
        });
        btnModificar.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        btnModificar.setBounds(294, 285, 129, 29);
        frame.getContentPane().add(btnModificar);

        JButton btnEliminar = new JButton("Eliminar");
        btnEliminar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                figuras.remove(figuraAmodificarEliminar);
                llenarGrilla(figuras);
                limpiarCampos();
                figuraAmodificarEliminar = null;
            }
        });
        btnEliminar.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        btnEliminar.setBounds(294, 325, 129, 29);
        frame.getContentPane().add(btnEliminar);

        JButton btnLimpiarCampos = new JButton("Limpiar campos");
        btnLimpiarCampos.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                limpiarCampos();
            }
        });
        btnLimpiarCampos.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        btnLimpiarCampos.setBounds(294, 365, 129, 29);
        frame.getContentPane().add(btnLimpiarCampos);

        JLabel lblNombre = new JLabel("Nombre");
        lblNombre.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblNombre.setBounds(10, 67, 99, 20);
        frame.getContentPane().add(lblNombre);

        textNombre = new JTextField();
        textNombre.setFont(new Font("Tahoma", Font.BOLD, 16));
        textNombre.setColumns(10);
        textNombre.setBounds(10, 86, 112, 20);
        frame.getContentPane().add(textNombre);

        JPanel panel_1 = new JPanel();
        panel_1.setLayout(null);
        panel_1.setBorder(new TitledBorder(null, "Dos variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panel_1.setBounds(168, 124, 262, 140);
        frame.getContentPane().add(panel_1);

        JLabel lblBase = new JLabel("Base");
        lblBase.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblBase.setBounds(10, 23, 60, 20);
        panel_1.add(lblBase);

        textBase = new JTextField();
        textBase.setFont(new Font("Tahoma", Font.BOLD, 16));
        textBase.setColumns(10);
        textBase.setBounds(10, 42, 112, 20);
        panel_1.add(textBase);

        JButton btnCrearRectangulo = new JButton("Rect\u00E1ngulo");
        btnCrearRectangulo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                figuras.add(new Rectangulo(textNombre.getText(), Float.parseFloat(textBase.getText()),
                        Float.parseFloat(textAltura.getText())));
                llenarGrilla(figuras);
                limpiarCampos();

            }
        });
        btnCrearRectangulo.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        btnCrearRectangulo.setBounds(74, 73, 112, 25);
        panel_1.add(btnCrearRectangulo);

        JLabel lblAltura = new JLabel("Altura");
        lblAltura.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblAltura.setBounds(139, 23, 60, 20);
        panel_1.add(lblAltura);

        textAltura = new JTextField();
        textAltura.setFont(new Font("Tahoma", Font.BOLD, 16));
        textAltura.setColumns(10);
        textAltura.setBounds(139, 42, 112, 20);
        panel_1.add(textAltura);

        JButton btnCrearTriangulo = new JButton("Tri\u00E1ngulo");
        btnCrearTriangulo.setBounds(74, 104, 112, 25);
        panel_1.add(btnCrearTriangulo);
        btnCrearTriangulo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                figuras.add(new Triangulo(textNombre.getText(), Float.parseFloat(textBase.getText()),
                        Float.parseFloat(textAltura.getText())));
                llenarGrilla(figuras);
                limpiarCampos();

            }
        });
        btnCrearTriangulo.setFont(new Font("Segoe UI", Font.PLAIN, 12));

        JPanel panel_2 = new JPanel();
        panel_2.setLayout(null);
        panel_2.setBorder(new TitledBorder(null, "Tres variables", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panel_2.setBounds(10, 275, 267, 125);
        frame.getContentPane().add(panel_2);

        JLabel lblValor_2 = new JLabel("Lado");
        lblValor_2.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblValor_2.setBounds(10, 26, 54, 20);
        panel_2.add(lblValor_2);

        textField_2 = new JTextField();
        textField_2.setFont(new Font("Tahoma", Font.BOLD, 16));
        textField_2.setColumns(10);
        textField_2.setBounds(10, 45, 112, 20);
        panel_2.add(textField_2);

        JButton btnCrearCuadrado_2 = new JButton("Crear poligono");
        btnCrearCuadrado_2.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        btnCrearCuadrado_2.setBounds(142, 85, 112, 25);
        panel_2.add(btnCrearCuadrado_2);

        JLabel lblValor_2_1 = new JLabel("Cantidad de lados");
        lblValor_2_1.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblValor_2_1.setBounds(10, 71, 176, 20);
        panel_2.add(lblValor_2_1);

        textField_4 = new JTextField();
        textField_4.setFont(new Font("Tahoma", Font.BOLD, 16));
        textField_4.setColumns(10);
        textField_4.setBounds(10, 90, 112, 20);
        panel_2.add(textField_4);

        JLabel lblValor_2_2 = new JLabel("Apotema");
        lblValor_2_2.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblValor_2_2.setBounds(142, 26, 74, 20);
        panel_2.add(lblValor_2_2);

        textField_5 = new JTextField();
        textField_5.setFont(new Font("Tahoma", Font.BOLD, 16));
        textField_5.setColumns(10);
        textField_5.setBounds(142, 45, 112, 20);
        panel_2.add(textField_5);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 418, 420, 173);
        frame.getContentPane().add(scrollPane);

        tablFiguras = new JTable();
        tablFiguras.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                figuraAmodificarEliminar = figuras.get(tablFiguras.getSelectedRow());

                if (figuraAmodificarEliminar instanceof Cuadrado)
                    asignarValores((Cuadrado) figuraAmodificarEliminar);
                else if (figuraAmodificarEliminar instanceof Circulo)
                    asignarValores((Circulo) figuraAmodificarEliminar);
                else if (figuraAmodificarEliminar instanceof Rectangulo)
                    asignarValores((Rectangulo) figuraAmodificarEliminar);
                else if (figuraAmodificarEliminar instanceof Triangulo)
                    asignarValores((Triangulo) figuraAmodificarEliminar);
            }
        });
        tablFiguras.setFont(new Font("Segoe UI", Font.PLAIN, 13));

        scrollPane.setViewportView(tablFiguras);

        JLabel lblNewLabel_1 = new JLabel("Maxima superficie");
        lblNewLabel_1.setFont(new Font("Segoe UI", Font.PLAIN, 12));
        lblNewLabel_1.setBounds(168, 67, 242, 20);
        frame.getContentPane().add(lblNewLabel_1);

        lblSuperficieMaxima = new JLabel("");
        lblSuperficieMaxima.setOpaque(true);
        lblSuperficieMaxima.setFont(new Font("Segoe UI", Font.BOLD, 20));
        lblSuperficieMaxima.setBounds(168, 82, 141, 25);
        frame.getContentPane().add(lblSuperficieMaxima);
        asignarValoresIniciales();
    }

    private void asignarValores(Cuadrado pCua) {
        textNombre.setText(pCua.getNombre());
        textValor.setText(Float.toString(pCua.getLado()));

    }

    private void asignarValores(Circulo pCir) {
        textNombre.setText(pCir.getNombre());
        textValor.setText(Float.toString(pCir.getRadio()));
    }

    private void asignarValores(Rectangulo pRec) {
        textNombre.setText(pRec.getNombre());
        textBase.setText(Float.toString(pRec.getBase()));
        textAltura.setText(Float.toString(pRec.getAltura()));
    }

    private void asignarValores(Triangulo pTri) {
        textNombre.setText(pTri.getNombre());
        textBase.setText(Float.toString(pTri.getBase()));
        textAltura.setText(Float.toString(pTri.getAltura()));
    }

    private void llenarGrilla(List<Figura> pFiguras) {
        int fila = 0;
        arrayfiguras = new String[pFiguras.size()][4];
        DecimalFormat df = new DecimalFormat("#.##");
        for (Figura figura : pFiguras) {
            for (int col = 0; col < 4; col++) {
                switch (col) {
                    case 0:
                        arrayfiguras[fila][col] = figura.getNombre();
                        break;
                    case 1:
                        arrayfiguras[fila][col] = figura.getValores();
                        break;
                    case 2:
                        arrayfiguras[fila][col] = df.format(figura.calcularPerimetro());
                        break;
                    case 3:
                        arrayfiguras[fila][col] = df.format(figura.calcularSuperficie());
                        break;

                    default:
                        break;
                }
            }
            fila++;
        }
        lblSuperficieMaxima.setText(df.format(Figura.getMaximaSuperficie()));
        tablFiguras.setModel(
                new DefaultTableModel(arrayfiguras, new String[] { "Nombre", "valores", "perimetro", "superficie" }));

    }

    private void asignarValoresIniciales() {
        figuras = new ArrayList<Figura>();
        figuras.add(new Cuadrado("Square", 69));
        figuras.add(new Circulo("Circle", 11));
        llenarGrilla(figuras);
    }

    private void limpiarCampos() {
        textNombre.setText("");
        textValor.setText("");
        textBase.setText("");
        textAltura.setText("");

    }
}
