package calculator.modelo.exception;

public class FiguraException extends Exception {
    public FiguraException(String message) {
        super(message);
    }
}
